const conf = require('./gulp.conf');

module.exports = function () {
  return {
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      routes: {
        '/bower_components': 'bower_components'
      }
    },
    open: false,
    ui: {
      port: 8081
    },
    host: '0.0.0.0',
    port: 8080
  };
};
