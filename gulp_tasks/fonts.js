const gulp = require('gulp');
const mainBowerFiles = require('main-bower-files');

const conf = require('../conf/gulp.conf');

gulp.task('fonts', fonts);

function fonts() {
  return gulp.src(mainBowerFiles('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe(gulp.dest(conf.path.tmp('styles')))
    .pipe(gulp.dest(conf.path.dist('styles')));
}