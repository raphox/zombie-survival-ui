# ZSSN (Zombie Survival Social Network)

The world as we know it has fallen into an apocalyptic scenario. A laboratory-made virus is transforming human beings and animals into zombies, hungry for fresh flesh.

You, as a zombie resistance member (and the last survivor who knows how to code), was designated to develop a system to share resources between non-infected humans.

## About

This project is a client to the project https://bitbucket.org/raphox/zombie-survival.

### Development tools

* https://angularjs.org/ Version 1.5.*
* http://gulpjs.com/
* https://bower.io/
* http://fountainjs.io/
  * https://github.com/FountainJS/generator-fountain-angular1
  * https://github.com/FountainJS/generator-fountain-gulpjs
  * https://github.com/FountainJS/generator-fountain-inject

## Developers

## First time

```
npm install
bower install
```

## How to run

```
gulp or gulp build
gulp serve
gulp serve:dist
gulp test
gulp test:auto
```

See more in http://fountainjs.io/doc/usage