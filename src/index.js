/* global angular */
/* global CommonsService */
/* global TodoService */

angular
  .module('app', ['ui.router', 'ngMaterial', 'ngMessages', 'restangular', 'angular-loading-bar'])
  .config(function (RestangularProvider, cfpLoadingBarProvider, $mdThemingProvider, $urlRouterProvider) {
    // TODO: Move to a config file
    RestangularProvider.setBaseUrl('http://zombie-survival-raphox.c9users.io:8080/api/v1');
    RestangularProvider.setRequestSuffix('.json');

    cfpLoadingBarProvider.includeSpinner = false;

    $mdThemingProvider.theme('success');
    $mdThemingProvider.theme('danger');
  })
  .run(function (Restangular, $mdDialog) {
    Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {
      if (response.status !== 422) {
        $mdDialog.show(
          $mdDialog.alert({
            title: 'Error: ' + response.status,
            textContent: response.statusText,
            ok: 'Close'
          })
        );
      }
    });
  })
  .service('commonsService', CommonsService);
