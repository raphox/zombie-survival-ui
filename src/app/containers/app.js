/* global angular */

/**
 * app component used to manager survivors in service
 *
 * @author: Raphael A. Araujo <raphox.araujo@gmail.com>
 */
angular
  .module('app')
  .component('app', {
    bindings: {survivors: '<'},
    templateUrl: 'app/containers/app.html',
    controller: App,
    transclude: true
  });

/**
 * Class of component to render survivor list
 */
function App($state, $mdSidenav) {
  this.$state = $state;
  this.$mdSidenav = $mdSidenav;
  this.survivorsFilterd = [];
}

App.prototype = {
  // Initilize filtered data in list of main page
  $onInit: function () {
    this.survivorsFilterd = this.survivors;
  },

  // Filter itens in list of main page
  filter: function (query) {
    this.survivorsFilterd = _.filter(this.survivors, query);
  },

  // Show a selected survivor
  show: function (survivor) {
    this.$state.go('app.item', {survivorId: survivor.id});
  },

  // Show form to create a new survivor
  new: function (survivor) {
    this.$state.go('app.item', {survivorId: null});
  }
};
