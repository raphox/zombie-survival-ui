/* global angular */

/**
 * CommonsService shared utils services with others class
 *
 * @author: Raphael A. Araujo <raphox.araujo@gmail.com>
 */
function CommonsService($mdToast, survivorFactory) {
  this.$mdToast = $mdToast;
  this.survivorFactory = survivorFactory;
}

CommonsService.prototype = {
  // Validate form and update or create a model in the respective factory
  save: function (form, model, callback) {
    var self = this;
    var result = false;

    // Clear messages from factory before validate again
    angular.forEach(model, function (value, key) {
      if (form[key]) {
        form[key].$setValidity("invalid", null);
      }
    });

    if (form.$valid) {
      // Model with ID represent a old model and without is a new
      var call = model.id ? model.put() : this.survivorFactory.post(model);

      call.then(function () {
        self.$mdToast.show(
          self.$mdToast.simple()
            .textContent('Success!')
            .position('top right')
            .theme('success')
            .hideDelay(3000)
        );

        callback(true);
      }, function (error) {
        self.$mdToast.show(
          self.$mdToast.simple()
            .textContent('Error! Try again later.')
            .position('top right')
            .theme('danger')
            .hideDelay(3000)
        );

        // Set errors messagens in form object
        angular.forEach(error.data, function (value, key) {
          form[key].$setValidity("invalid", false);
        });
      });
    }

    callback(result);
  }
};
