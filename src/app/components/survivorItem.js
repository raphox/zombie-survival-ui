/* global angular */

/**
 * survivorItem component used to manager survivors item in service
 *
 * @author: Raphael A. Araujo <raphox.araujo@gmail.com>
 */
angular
  .module('app')
  .component('survivorItem', {
    bindings: {
      survivor: '<',
      resources: '<'
    },
    templateUrl: 'app/components/survivorItem.html',
    controller: SurvivorItem
  });

/**
 * Class of component to render survivor item
 */
function SurvivorItem($scope, $state, $mdSidenav, $mdDialog, commonsService, survivorFactory) {
  this.$scope = $scope;
  this.$state = $state;
  this.$mdSidenav = $mdSidenav;
  this.$mdDialog = $mdDialog;
  this.commonsService = commonsService;
  this.survivorFactory = survivorFactory;

  this.resourceToTrade = null;
}

SurvivorItem.prototype = {
  // Show sidebar of survivor item
  $onInit: function () {
    var self = this;

    this.$mdSidenav("right").open();
    this.$mdSidenav("right").onClose(function () {
      self.$state.go('app', {}, {reload: true});
    });
  },

  // Send data of form to save action
  save: function () {
    var self = this;

    this.commonsService.save(this.$scope.form, this.survivor, function (result) {
      if (result) {
        self.close();
      }

      return result;
    });
  },

  // Send report infection to current user by session user
  report: function () {
    var self = this;

    this.$mdDialog.show({
      controller: self.DialogController,
      contentElement: '#reportDialog',
      clickOutsideToClose: true
    });
  },

  // Call dialog of trade form
  trade: function (resource) {
    var self = this;

    this.resourceToTrade = resource;

    this.$mdDialog.show({
      controller: self.TradeController,
      contentElement: '#tradeDialog',
      clickOutsideToClose: true
    }).then(function () {
      self.survivor.getList('resources_survivors').then(function (response) {
        self.resources = response;
      });
    });
  },

  // Close sidebar of survivor item
  close: function () {
    this.$mdSidenav("right").close();
  },

  /**
   * Used to report infection between current user and session user
   */
  DialogController: function ($scope, $mdDialog, $mdToast, survivorFactory) {
    $scope.survivors = [];

    // Load suvivor's options to form
    survivorFactory.getList().then(function (response) {
      $scope.survivors = response;
    });

    // Send data of form to report infection action
    $scope.done = function (survivor, reporter) {
      survivor.reportInfection(reporter).then(function () {
        $mdToast.show(
          $mdToast.simple()
            .textContent('Success!')
            .position('top right')
            .theme('success')
            .hideDelay(3000)
        );
      });

      $mdDialog.hide();
    };

    // close form dialog
    $scope.cancel = function () {
      $mdDialog.cancel();
    };
  },

  /**
   * Used to trade resources between current user and session user
   */
  TradeController: function ($scope, $mdDialog, $mdToast, survivorFactory, resourceFactory) {
    $scope.survivors = [];
    $scope.resourcesAll = [];

    // Load survivor's options to form
    survivorFactory.getList().then(function (response) {
      $scope.survivors = response;
    });

    // Load resource's options to form
    resourceFactory.getList().then(function (response) {
      $scope.resourcesAll = response;
    });

    // Send data of form to trade action
    $scope.done = function (survivor, survivorGiverId, resourceGiver, resourceReceiver) {
      survivor.trade(survivorGiverId, {id: resourceGiver}, {id: resourceReceiver}).then(function () {
        $mdToast.show(
          $mdToast.simple()
            .textContent('Success!')
            .position('top right')
            .theme('success')
            .hideDelay(3000)
        );

        // hide form dialog
        $mdDialog.hide();
      }, function (error) {
        var message = _.map(error.data, function (value, key) {
          return value;
        }).join("\n");

        $mdToast.show(
          $mdToast.simple()
            .textContent("Error! Check your data form:\n" + message)
            .position('top right')
            .theme('danger')
            .hideDelay(3000)
        );
      });
    };

    // close form dialog
    $scope.cancel = function () {
      $mdDialog.cancel();
    };
  }
};
