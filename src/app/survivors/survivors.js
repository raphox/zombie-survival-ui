/* global angular */

/**
 * survivorFactory factory used to generate REST functions to survivors
 *
 * @author: Raphael A. Araujo <raphox.araujo@gmail.com>
 */
angular
  .module('app')
  .factory('survivorFactory', function (Restangular) {
    Restangular.extendModel('survivors', function (model) {
      // REST action to report infection of survivors
      model.reportInfection = function (reporterId) {
        return this.customOperation('post', 'survivor_infections', {
          "survivor_infection[survivor_id]": this.id,
          "survivor_infection[reporter_id]": reporterId
        }, {'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"});
      };

      // REST action to trade resources
      // TODO: Possibility to use many resources
      model.trade = function (survivorGiverId, resourceGiver, resourceReceiver) {
        return this.all('resources_survivors')
          .customOperation('post', 'trade', {
            "resources_survivors[survivor_id]": this.id,
            "resources_survivors[survivor_giver_id]": survivorGiverId,
            "resources_survivors[resources_giver][0][id]": resourceGiver.id,
            "resources_survivors[resources_giver][0][quantity]": 1,
            "resources_survivors[resources_receiver][0][id]": resourceReceiver.id,
            "resources_survivors[resources_receiver][0][quantity]": 1
          }, {'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"});
      };

      return model;
    });

    return Restangular.service('survivors');
  });
