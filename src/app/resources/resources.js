/* global angular */
/* global Restangular */

/**
 * resourceFactory factory used to generate REST functions to resources
 *
 * @author: Raphael A. Araujo <raphox.araujo@gmail.com>
 */
angular
  .module('app')
  .factory('resourceFactory', function (Restangular) {
    return Restangular.service('resources');
  });
