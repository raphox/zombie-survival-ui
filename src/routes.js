/* global angular */

angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'app',
      resolve: {
        survivors: function (survivorFactory) {
          return survivorFactory.getList();
        }
      }
    })
    .state('app.item', {
      url: '{survivorId}',
      component: 'survivorItem',
      resolve: {
        survivor: function (survivorFactory, $transition$) {
          var survivor = {};

          if ($transition$.params().survivorId) {
            survivor = survivorFactory.one($transition$.params().survivorId).get();
          }

          return survivor;
        },

        resources: function (survivorFactory, $transition$) {
          var resources = {};

          if ($transition$.params().survivorId) {
            resources = survivorFactory.one($transition$.params().survivorId).getList('resources_survivors');
          }

          return resources;
        }
      }
    });
}
